import mongoose from "mongoose";

const UsersSchema = mongoose.Schema({
  id: { type: String, required: true },
  first_name: { type: String },
  last_name: { type: String },
  username: { type: String },
  follow: {
    us: { type: Boolean, default: false },
    eu: { type: Boolean, default: false }
  },
  created_at: {
    type: Date,
    default: new Date()
  }
}, { collection: 'users' });

const Users = mongoose.model('users', UsersSchema);

module.exports = Users;
