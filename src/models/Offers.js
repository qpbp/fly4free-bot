import mongoose from "mongoose";

const OffersSchema = mongoose.Schema({
  link: { type: String, unique: true },
  type: { type: String, trim: true, required: true, enum: ['eu', 'us'] },
  img: { type: String },
  title: { type: String },
  description: { type: String },
  date: { type: Date },
  created_at: {
    type: Date,
    default: new Date()
  }
}, { collection: 'offers' });

const Offers = mongoose.model('offers', OffersSchema);

module.exports = Offers;
