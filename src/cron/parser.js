import bluebird from "bluebird";
fetch.Promise = bluebird;
import cheerio from "cheerio";
import fetch from "node-fetch";
import co from "co";

const parse_data = [
  { link: "http://www.fly4free.com/cheap-flights/deals/europe/", type: "europe" },
  { link: "http://www.fly4free.com/cheap-flights/deals/usa-and-canada/", type: "usa_canada" }
];

const app_url = "https://damp-caverns-82135.herokuapp.com/offers";
// const app_url = "http://localhost:3000/offers";

co(function*() {
  let html_data = Promise.all(parse_data.map(elem => fetch(elem.link).then(resp => resp.text())));
  let modified_data = html_data.then(htmls => modify_data(htmls));
  let res = yield [html_data, modified_data];
  console.log(res[1])
  let send_data = yield fetch(app_url, {
    method: 'POST',
    body: JSON.stringify(res[1]),
    headers: { 'content-type': 'application/json'}
  });
  let json = yield send_data.json();
  console.log(json);
}).catch(onerror);

function modify_data(htmls) {
  let cheerio_ctx = htmls.map(tmpl => cheerio.load(tmpl));

  let data = cheerio_ctx.map((ctx, cheerio_index) => {
    let innerData = [];
    ctx('.entries > .entry').each(function(index) {

      let self = ctx(this);

      let img = self.children('.media').children('a').children('img').attr("src");

      let title = self.children('.entry__title').children('a').text();

      let description = self.children('.entry__content').children('p').children('strong').text();

      let date = self.children('.meta').children('.date').text();

      let link = self.children('.entry__title').children('a').attr('href');

      innerData.push({
        "img": img,
        "title": title,
        "description": description,
        "date": new Date(date),
        "link": link
      });

    });

    return {
      "data": innerData,
      "type": parse_data[cheerio_index].type
    };

  });

  let data_obj = {};

  data.map(obj => {
    //sort by date
    obj.data.sort(function(a, b) {
      return new Date(b.date) - new Date(a.date);
    });

    data_obj[obj.type] = obj.data;
  });

  return data_obj;
};

function onerror(err) {
  // log any uncaught errors
  // co will not throw any errors you do not handle!!!
  // HANDLE ALL YOUR ERRORS!!!
  console.error(err.stack);
};
