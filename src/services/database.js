'use strict'
import bluebird from "bluebird";
mongoose.Promise = bluebird;
import mongoose from "mongoose";

mongoose.connect('mongodb://roman:1234567@ds061371.mlab.com:61371/game');

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => {
  console.log("DB successfully connected")
});

module.exports = mongoose;