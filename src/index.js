// import Telegram from "node-telegram-bot-api";
import Telegram from "telegram-node-bot";
const token = "190886077:AAFgeErwOu6P08qo19RFuhIn1wb-Uay8kSA";
const tg = Telegram(token);
import mongoose from "./services/database"
import Commands from "./commands/list";

import bodyParser from 'body-parser';
import express from 'express';
import loki from 'lokijs';
import Users from './models/Users';
import Offers from './models/Offers';
import co from "co";
const port = process.env.PORT || 3000;

tg.router.
when(['/start'], 'StartController').
when(['/follow_eu'], 'RegionController').
when(['/follow_us'], 'RegionController').
when(['/unfollow_eu'], 'RegionController').
when(['/unfollow_us'], 'RegionController');
// when(['/restart'], 'RestartController')

tg.controller('StartController', ($) => {

  tg.for('/start', ($) => {
    let t_user = $.user;
    console.log(t_user)
    co(function*() {
      try {
        let findUser = yield Users.findOne({ id: t_user.id }).exec();

        if (findUser) {
          let message = 'Hi, again, ' + findUser.first_name;
          $.sendMessage(message);
        } else {
          let createdUser = yield Users.create(t_user);
          let message = 'Yo, seems you are new' + createdUser.first_name;

          $.sendMessage(message);
        }

      } catch (err) {
        console.error(err.message); // "boom" 
      }
    }).catch(onerror);
  })
});

tg.controller('RegionController', ($) => {
  let messageText = $.message.text;

  let commands = {
    '/follow_eu': { update: { 'follow.eu': true }, text: "Now you follow Europe" },
    '/unfollow_eu': { update: { 'unfollow.eu': false }, text: "Now you unfollow Europe" },
    '/follow_us': { update: { 'follow.us': true }, text: "Now you follow Usa and Canada" },
    '/unfollow_us': { update: { 'unfollow.us': false }, text: "Now you unfollow Usa and Canada" }
  };

  let followObject = {};

  if (messageText in commands) {
    followObject = commands[messageText];
  }

  console.log(followObject);

  tg.for(messageText, ($) => {
    let t_user = $.user;

    co(function*() {
      console.log(followObject)
      try {
        let updatedUser = yield Users.findOneAndUpdate({ id: t_user.id }, { $set: followObject.update }, { new: true }).exec();
        $.sendMessage(followObject.text);
      } catch (err) {
        console.error(err.message); // "boom" 
      }
    }).catch(onerror);
  });

});


//init express for listening and html
const app = express();

app.use(bodyParser.json()); // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({ // to support URL-encoded bodies
  extended: true
}));

app.get('/', (req, res) => {
  res.send("Working");
});

app.post('/offers', (req, res) => {
  //working

  let euNewOffers = req.body.europe || [];
  let usNewOffers = req.body.usa_canada || [];

  let newOffers = [usNewOffers, euNewOffers];
  console.log("NewOffers", newOffers);

  co(function*() {
    try {

      let usFoundOffers = Offers.find({ type: "us" }).sort({ date: -1 }).limit(10).exec();
      let euFoundOffers = Offers.find({ type: "eu" }).sort({ date: -1 }).limit(10).exec();

      let foundOffers = yield [usFoundOffers, euFoundOffers];
      console.log("foundOffers", foundOffers);

      let uniqueOffers = { eu: [], us: [] };

      foundOffers[0].map(usFoundOffer => {
        usNewOffers.map(usNewOffer => {
          if (usFoundOffer.link != usNewOffer.link) {
            usNewOffer.type = "us";
            uniqueOffers.us.push(usNewOffer);
          }
        });
      });

      foundOffers[1].map(euFoundOffer => {
        euNewOffers.map(euNewOffer => {
          if (usFoundOffer.link != usNewOffer.link) {
            usNewOffer.type = "eu";
            uniqueOffers.eu.push(usNewOffer);
          }
        });
      });

      console.log("usUniqueOffers", uniqueOffers.us);
      console.log("euUniqueOffers", uniqueOffers.eu);

      let euUsers = yield Users.find({ "follow.eu": true }).exec();
      let usUsers = yield Users.find({ "follow.us": true }).exec();
      console.log("users", euUsers, usUsers);

      uniqueOffers.us.map(offer => {
        usUsers.map(usUser => {
          tg.sendMessage(usUser.id, offer.link);
        });
      });
      console.log('send US');
      uniqueOffers.eu.map(offer => {
        euUsers.map(euUser => {
          tg.sendMessage(euUser.id, offer.link);
        });
      });

      yield Users.create(uniqueOffers.us);
      yield Users.create(uniqueOffers.eu);

    } catch (err) {
      console.error(err.message); // "boom" 
    }
  }).catch(onerror);

  res.send({ message: "ok" });
});

function onerror(err) {
  // log any uncaught errors 
  // co will not throw any errors you do not handle!!! 
  // HANDLE ALL YOUR ERRORS!!! 
  console.error(err.stack);
}

app.listen(port, () => {
  console.log('App listening on port' + port);
});
